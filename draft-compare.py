#!/usr/bin/env python
#
# PLANNED FEATURES IN SQUARE BRACKETS: [...]
#
# Pass this two different versions of a LaTeX document, and it will collect figures [and tables] and make slides comparing them side by side.
# [Figures and tables are matched based on their LaTeX `\\label`, with subfigures being resolved and split.]
#
# Implementation information (read this if you run into failures or weird behaviour)
# ==================================================================================
# 
# [Subfigures are resolved simply by checking for multiple `\\includegraphics` commands within the same `figure` environment.]
# 
# [Notes are matched by `\\label` (and possibly Roman letter 'a', 'b', 'c', ... if there are multiple subfgures) and should be given in the form:]
#
# [fig:my_figure a "Some \\LaTeX text to add."]
# [fig:my_figure b "Another annotation."]
# [fig:nice_figure "Figure is now better."]
# [tab:some_table "The value of $x$ has changed."]
#

import argparse
import glob
import os
import subprocess
import uuid

# Leading program info text for invocation with -h / --help argument
program_description = '''\
Compare the plots and tables in two drafts side-by-side by producing LaTeX
Beamer slides of them (in .tex format, to be compiled by the user.)
'''

# Trailing program info text for invocation with -h / --help argument
program_epilogue = '''\
Implementation details (read in case of problems):

HINT: in case of any problems, run with the -v option and study the output.

Plots are identified by the "\\includegraphics" command
Tables are identified by the "\\begin{tabular}" and "\\end{tabular}" commands
NOTE: If these commands are hidden away in \\include{}'d \\input{}'d files,
this program will not find them.

Tables will be placed in ./difftables/ (created if doesn't exist) as .tex
files. If their LaTeX compilation succeeds, they will also be present as .pdf
files and included in the slides. If LaTeX compilation of tables fails, you
can try setting the environment variable MYLATEXINVOCATION to your pdflatex
invocation, containing the string "{0}" that will be replaced by the filename
(without extension), e.g.
export MYLATEXINVOCATION=/my/awesome/pdflatex -halt-on-error -interaction=nonstopmode {0}
'''

# You can add your own invocations if none of these works for you
# Or set the environment variable MYLATEXINVOCATION to the desired
# invocation (e.g. MYLATEXINVOCATION='/my/awesome/pdflatex -halt-on-error -interaction=nonstopmode {0}')
#
# You probably do yourself a favour if you set pdflatex to exit when 
pdflatex_invocations = [
    '$MYLATEXINVOCATION',
    'pdflatex -halt-on-error -interaction=nonstopmode {0}',
    '/usr/texbin/pdflatex -halt-on-error -interaction=nonstopmode {0}',
    '/opt/local/bin/pdflatex -halt-on-error -interaction=nonstopmode {0}', # on Macs using MacPorts
]

frontmatter = '''\
\\documentclass[9pt,aspectratio=169]{beamer}

\\usetheme{default}
\\setbeamertemplate{navigation symbols}{} 
\\usepackage{parskip}
\\usepackage{subfig}
\\usepackage[labelformat=empty]{caption}
\\usepackage{graphicx}
\\graphicspath{ {./diffplots/} }

\\begin{document}
'''

slidetext = '''
\\begin{{frame}}{{}}
\\includegraphics[width=0.47\\textwidth]{{{0}}}
\\hspace{{0.05\\textwidth}}
\\includegraphics[width=0.47\\textwidth]{{{1}}}
\\end{{frame}}
'''

backmatter = '''
\\end{document}
'''



def read_preamble(filepath, verbose=False):
    latex = ''
    with open(filepath, 'r') as infile:
        for line in infile:
            if '\\begin{document}' in line:
                return latex
            if '\\input{' in line:
                continue
            if '\\documentclass' in line and not '%' in line.split('documentclass')[0]:
                latex += line.split('{')[0] + '{standalone}' + line.split('}')[1]
            else:
                latex += line



def compile_document(invocation, directory, verbose=False):
    stdout = None if verbose else subprocess.PIPE
    stderr = None if verbose else subprocess.PIPE
    p = subprocess.Popen(invocation.split(), cwd=directory, stdout=stdout, stderr=stderr)
    p.wait()
    return p.returncode == 0



def try_until_pdflatex_worked(target, directory, verbose=False):
    for pdflatex in pdflatex_invocations:
        if verbose:
            print 'Trying command', pdflatex.format(target)
        try:
            success = compile_document(pdflatex.format(target), directory, verbose=verbose)
            if success:
                return
        except OSError:
            if verbose:
                print 'Command "{0}" led to an OSError (perhaps does not exist)'.format(pdflatex)
    raise RuntimeError('Did not manage to compile LaTeX document :(')



def table_to_pdf(tablelatex, filepath, verbose=False):
    # Get the preamble of the .tex document
    # Copy it into a new .tex file at the same location (generate a unique file name to avoid name clashes)
    # Change the document type to 'standalone'
    # Try to compile it
    # Copy the PDF (if compilation succeeded) and .tex into a directory called $PWD/difftables
    # Delete all the other files related to the standalone table
    # If the PDF was created successfully, include it in the slides as a plot
    standalone_name = 'table-' + str(uuid.uuid4())
    directory = os.path.dirname(filepath)
    with open(os.path.join(directory, standalone_name + '.tex'), 'w') as tablefile:
        tablefile.write(read_preamble(filepath, verbose=verbose))
        tablefile.write('\\begin{document}\n')
        tablefile.write(tablelatex.replace('\\pagebreak', ''))
        tablefile.write('\\end{document}\n')
    try:
        try_until_pdflatex_worked(standalone_name, directory, verbose=verbose)
        if not os.path.exists('difftables'):
            os.makedirs('difftables')
        os.system('mv ' + os.path.join(directory, standalone_name + '.pdf difftables'))
    except RuntimeError:
        print 'Compiling LaTeX table failed. Ignoring.'
        return ''
    try:
        os.system('mv ' + os.path.join(directory, standalone_name + '.tex difftables'))
        os.system('rm ' + os.path.join(directory, standalone_name + '*'))
    except:
        pass
    return standalone_name + '.pdf'



# Extract locations where to look for the plots/tables from each draft
# The tables from both draft will be in the same location, with unique
# names (because they are compiled into their own standalone PDFs for
# inlusion in the slides)
def extract_graphics_search_paths(filepath, verbose=False):
    filedir = os.path.dirname(filepath)
    search_paths = [filedir, 'difftables']
    return search_paths



def extract_plot_and_table_paths(filepath, ignore_after, ignore_before, verbose=False):
    plotpaths = []
    current_table = ''
    ignoring = bool(ignore_before)
    with open(filepath, 'r') as infile:
        for line in infile:
            if ignoring and ignore_before in line:
                ignoring = false
            if ignoring:
                continue
            if current_table:
                current_table += line
            if 'includegraphics' in line:
                if '%' in line.split('includegraphics')[0]: continue # This table is commented out
                plotpath = line.split('includegraphics')[1].split('{')[1].split('}')[0]
                plotpaths.append(plotpath)
            if ignore_after and ignore_after in line:
                return plotpaths
            if 'begin{tabular}' in line:
                current_table += line
            if 'end{tabular}' in line:
                tablepath = table_to_pdf(current_table, filepath, verbose=verbose)
                if tablepath:
                    plotpaths.append(tablepath)
                current_table = ''
    return plotpaths



def find_plot(plotpath, graphicspaths, verbose=False):
    triedpaths = []
    for graphicspath in graphicspaths:
        fullpath = os.path.join(graphicspath, plotpath)
        triedpaths.append(fullpath)
        if os.path.exists(fullpath) or glob.glob(fullpath + '.*'):
            # ignore extension when checking if file exists, because LaTeX allows omission of the extension in \includegraphics
            return fullpath
    raise RuntimeError('Could not find plot "{0}" at any of the tried locations: {1}'.format(plotpath, ', '.join(triedpaths)))



def get_plot_paths(filepath, ignore_after, ignore_before, verbose=False):
    graphics_search_path = extract_graphics_search_paths(filepath, verbose=verbose)
    plot_paths = extract_plot_and_table_paths(filepath, ignore_after, ignore_before, verbose=verbose)
    return [find_plot(plot_path, graphics_search_path, verbose=verbose) for plot_path in plot_paths]



def write_beamer_slides(aplotpaths, bplotpaths, outfilename='diffslides.tex', verbose=False):    
    with open(outfilename, 'w') as outfile:
        outfile.write(frontmatter)
        for a, b in zip(aplotpaths, bplotpaths):
            outfile.write(slidetext.format(a, b))
        outfile.write(backmatter)



def main():
    parser = argparse.ArgumentParser(description=program_description, epilog=program_epilogue, formatter_class=argparse.RawDescriptionHelpFormatter)
    parser.add_argument('left', help='Left-hand-side draft .tex file', nargs=1)
    parser.add_argument('right', help='Right-hand-side draft .tex file', nargs=1)
    parser.add_argument('-o', '--outfile', help='Output .tex file name', type=str, default='diffslides.tex')
    #parser.add_argument('-n', '--notes', help='Annotation .tex file name [NOT YET IMPLEMENTED]', type=str, default='')
    parser.add_argument('-a', '--ignore-after', dest='ignore_after', help='Ignore .tex input after this string', type=str, default='')
    parser.add_argument('-b', '--ignore-before', dest='ignore_before', help='Ignore .tex input before this string', type=str, default='')
    parser.add_argument('-v', '--verbose', help='print extra information to help debugging', default=False, action="store_true")
    #parser.add_argument('-v', '--verbose', help='print extra information to help debugging', default=False, action="store_true")
    args = parser.parse_args()
    
    a_plotpaths = get_plot_paths(args.left[0], args.ignore_after, args.ignore_before, verbose=args.verbose)
    b_plotpaths = get_plot_paths(args.right[0], args.ignore_after, args.ignore_before, verbose=args.verbose)
    write_beamer_slides(a_plotpaths, b_plotpaths, args.outfile, verbose=args.verbose)

if __name__ == '__main__':
    main()