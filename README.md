# Draft-compare

Slick tool for making side-by-side comparisons of plots and tables in two LaTeX draft versions. Plots and tables are placed in LaTeX Beamer slides, which you can then complete with comments on the changes etc. Let the computer do the work, more time for you to do interesting things.

## Usage

Download `draft-compare.py`

```bash
wget https://gitlab.cern.ch/strichte/draft-compare/raw/master/draft-compare.py
# or
git clone ssh://git@gitlab.cern.ch:7999/strichte/draft-compare.git
```

Make it executable

```bash
chmod +x draft-compare.py
```

Show usage information and command line options

```bash
./draft-compare.py -h
```

The help printout also includes information about implementation details and known shortcomings.

## Requirements

* Python 2.7
* pdflatex, if you wish to include tables in the comparison and create a PDF of the slides. Even without pdflatex working, you get a LaTeX source file of the slides including the plots.

## Limitations

The biggest limitation is currently that the tool **expects the plots and tables in both drafts to correspond one to one**, i.e. to be present in the same number and same order. If this is not the case, the following will work for now:

* Make a copy of the draft with extra plots/tables and comment them out
* Use the modified draft for the automatic comparison
* If desired, add the new plots to the slides by hand

The second biggest limitation is that LaTeX `\input` and `\include` commands are not followed. This means that plots and tables included via these commands will not appear in the comparison. (Note: at table containing `\input` commands inside the `tabular` environment will work fine, as long as the `tabular` environment is directly written into the main document.)

*Please report any bugs and undocumented non-obvious limitations to me. You can also send me feature requests, there are still a lot of improvements that can be made.*
